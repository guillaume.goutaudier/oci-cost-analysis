import oci
import os

#--------- Configuration variables -------				
reporting_namespace = 'bling'
prefix = ""
config_file = "~/.oci/config"
config_section = "DEFAULT"
destintation_path = 'reports'
				
# Make a directory to receive reports
if not os.path.exists(destintation_path):
    os.mkdir(destintation_path)
				
# Get the list of reports
config = oci.config.from_file(config_file,config_section)
reporting_bucket = config['tenancy']
object_storage = oci.object_storage.ObjectStorageClient(config)
report_bucket_objects = object_storage.list_objects(reporting_namespace, reporting_bucket, prefix=prefix, fields='name,etag,timeCreated,md5,timeModified')
				
for o in report_bucket_objects.data.objects:
    print('Found file ' + o.name)
    filename = o.name.rsplit('/', 1)[-1]
    day_str = o.time_created.strftime("%Y%m%d")
    target_path = destintation_path + '/' + day_str
    target_filename = target_path + '/' + filename
   
    if not os.path.exists(target_path):
        os.mkdir(target_path)
 
    object_details = object_storage.get_object(reporting_namespace, reporting_bucket, o.name)
    with open(target_filename, 'wb') as f:
        for chunk in object_details.data.raw.stream(1024 * 1024, decode_content=False):
            f.write(chunk)
    print('----> saved as '+target_filename)

