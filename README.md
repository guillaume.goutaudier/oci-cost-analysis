# OCI Cost Analysis
The objective of this project is to give a comprehensive procedure on how one can export cost/consumption data from the Oracle Cloud Infrastructure (OCI). 

We will do that using a modified version of the Python script provided in the Oracle documentation.

Tests have been done on MacOS but the procedure should work on other platforms with minor adaptations.


# How it works
- For each tenancy, reports are generated on an Oracle managed storage location called 'bling'. This is the same location for every tenant. All reports are stored on a bucket with the same name as the tenancy.
- Privileges to access the reports can be granted to any group of users.
- To access the reports, users can use the UI, CLI, or SDK

The architecture therefore looks like the following:
![architecture.png](architecture/architecture.png)


# Pre-requisite: OCI CLI
The Python SDK relies on API Keys and configuration files that can easily be generated using the OCI CLI.

Follow the official instructions to install the OCI CLI. On Mac OS X you can simply use Homebrew:
```
brew update && brew install oci-cli
oci setup config
# accept all defaults and provide requested information from the OCI console
# !!! WARNING: the provided region must be your "home" region !!!
```

Upload the public key to the OCI console in your `User Settings` and check the MD5 matches.


# Install the Pyhton SDK
Installating the python SDK is as easy as installing the oci python module:
```
pip3 install oci
```

After installation, check that everything works well from the python console:
```
>>> import oci
# Set up config
>>> config_file = "~/.oci/config"
>>> config_section = "DEFAULT"
>>> config = oci.config.from_file(config_file,config_section)
>>> identity = oci.identity.IdentityClient(config)
>>> user = identity.get_user(config["user"]).data
>>> print(user)
```


# Create IAM policy 
Create the following IAM policy to users in your tenancy to read cost reports from the Oracle-managed OCI tenancy:
```
define tenancy reporting_tenancy as ocid1.tenancy.oc1..aaaaaaaaned4fkpkisbwjlr56u7cj63lf3wffbilvqknstgtvzub7vhqkggq
endorse group <replace_with_your_group_name> to read objects in tenancy reporting_tenancy
```


# Download reports
Execute the python scripts:
```
python3 download_reports.py
```
The script will download all the reports to a `reports` folder, in a subfolder that corresponds to the date of the report e.g. `20201204`


# References
https://github.com/oracle/oci-python-sdk

https://docs.cloud.oracle.com/en-us/iaas/Content/API/SDKDocs/pythonsdk.htm#SDK_for_Python

https://docs.cloud.oracle.com/en-us/iaas/Content/Billing/Tasks/accessingusagereports.htm


